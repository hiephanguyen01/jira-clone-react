import React from 'react'
import styles from "./Home.module.scss"
import classNames from 'classnames/bind'

const cx = classNames.bind(styles)

export const Home = () => {
  return (
    <div>Home</div>
  )
}
