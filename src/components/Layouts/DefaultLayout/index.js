import React, { useEffect } from "react";
import styles from "./DefaultLayout.module.scss";
import classNames from "classnames/bind";
import { Header } from "./Header";
import { Sidebar } from "../Sidebar";
import { USER_LOGIN } from "util/contants";
import { useNavigate } from "react-router-dom";

const cx = classNames.bind(styles);

export const DefaultLayout = ({ children }) => {
  const navigate = useNavigate(false);
  useEffect(() => {
    if (!localStorage.getItem(USER_LOGIN)) {
      navigate("/login");
    }
  });

  return (
    <div className={cx("wrapper")}>
      <Sidebar />
      <div className={cx("container")}>
        <Header />
        <div className={cx("content")}>{children}</div>
      </div>
    </div>
  );
};
